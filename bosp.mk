
ifdef CONFIG_CONTRIB_TESTING_JAVA_TESTAPP

# Targets provided by this project
.PHONY: rtlib_java_testapp clean_rtlib_java_testapp

# Add this to the "contrib_testing" target
testing: rtlib_java_testapp
clean_testing: clean_rtlib_java_testapp

MODULE_CONTRIB_TESTING_JAVA_TESTAPP=contrib/testing/rtlib-java-testapp
JAVA_BINDINGS_LIBRARY_PATH=lib/bbque/bindings/java
APP_BIN_OUT=usr/bin
TESTAPP_NAME=RTLibTestEXC
CLASSPATH=$(BUILD_DIR)/$(JAVA_BINDINGS_LIBRARY_PATH)/RTLibJavaSdk.jar
JAVAC=`javac -version`
JAVAC_FLAGS=-cp $(CLASSPATH) -d build/$(BUILD_TYPE)

rtlib_java_testapp: external
	@echo
	@echo "==== Building RTLib Java Test Application ($(BUILD_TYPE)) ===="
# @echo " Compiler     : $(JAVAC)"
	@echo " Sysroot      : $(PLATFORM_SYSROOT)"
	@echo " Flags        : $(JAVAC_FLAGS)"
	@echo " Classpath    : $(CLASSPATH)"
	@echo " Build dir    : $(BUILD_DIR)/$(APP_BIN_OUT)"
	@[ -d $(MODULE_CONTRIB_TESTING_JAVA_TESTAPP)/build/$(BUILD_TYPE) ] || \
                mkdir -p $(MODULE_CONTRIB_TESTING_JAVA_TESTAPP)/build/$(BUILD_TYPE) || \
                exit 1
	@cd $(MODULE_CONTRIB_TESTING_JAVA_TESTAPP) && \
		CLASSPATH=.:$(CLASSPATH) && \
		`javac $(JAVAC_FLAGS) $(TESTAPP_NAME).java`
	@echo "==== Installing RTLib Java Test Application ($(BUILD_TYPE)) ===="
	@cp $(MODULE_CONTRIB_TESTING_JAVA_TESTAPP)/build/$(BUILD_TYPE)/$(TESTAPP_NAME).class \
		$(BUILD_DIR)/$(APP_BIN_OUT)/$(TESTAPP_NAME).class
	@echo "Install dir: $(BUILD_DIR)/$(APP_BIN_OUT)/$(TESTAPP_NAME).class"
	@echo "---- Done ----"	

clean_rtlib_java_testapp:
	@echo
	@echo "==== Clean-up RTLib Java Test Application ===="
	@[ ! -f $(BUILD_DIR)/$(APP_BIN_OUT)/$(TESTAPP_NAME).class ] || \
		rm -f $(BUILD_DIR)/$(APP_BIN_OUT)/$(TESTAPP_NAME).class; \
		rm -f $(MODULE_CONTRIB_TESTING_JAVA_TESTAPP)/build/$(BUILD_TYPE)/$(TESTAPP_NAME).class 
	@echo

else # CONFIG_CONTRIB_TESTING_JAVA_TESTAPP

rtlib_java_testapp:
	$(warning contib JTestApp module disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_CONTRIB_TESTING_JAVA_TESTAPP

