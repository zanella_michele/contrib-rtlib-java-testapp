import bbque.rtlib.RTLib;
import bbque.rtlib.enumeration.RTLibConstraintOperation;
import bbque.rtlib.enumeration.RTLibConstraintType;
import bbque.rtlib.enumeration.RTLibExitCode;
import bbque.rtlib.exception.RTLibException;
import bbque.rtlib.exception.RTLibRegistrationException;
import bbque.rtlib.model.BbqueEXC;
import bbque.rtlib.model.RTLibConstraint;
import bbque.rtlib.model.RTLibServices;

public class RTLibTestEXC extends BbqueEXC {

    public static void main(String[] args) {
        try {
            // initialize the library
            RTLibServices library = RTLib.init("simulator");
            // register the application
            RTLibTestEXC exc = new RTLibTestEXC("RTLibTestEXC",
                                                "BbqRTLibTestApp",
                                                library);
            // app successfully registered, calling start()
            exc.start();
            // app successfully started
            exc.waitCompletion();
            // execution completed!
        } catch (RTLibException e) {
            String format = "[main] error occurred: %s";
            String message = RTLib.getErrorStr(e.getExitCode());
            System.out.println(String.format(format, message));
        }
    }

    private RTLibTestEXC(String name, String recipe, RTLibServices library)
                                        throws RTLibRegistrationException {
        super(name, recipe, library);
    }

    @Override
    protected void onSetup() throws RTLibException {
        System.out.println("[SimulatorEXC] onSetup()");
    }

    @Override
    protected void onConfigure(int awmId) throws RTLibException {
        System.out.println("[SimulatorEXC] onConfigure(awmId: " + awmId + ")");
    }

    @Override
    protected void onSuspend() throws RTLibException {
        System.out.println("[SimulatorEXC] onSuspend()");
    }

    @Override
    protected void onResume() throws RTLibException {
        System.out.println("[SimulatorEXC] onResume()");
    }

    @Override
    protected void onRun() throws RTLibException {
        System.out.println("[SimulatorEXC] onRun()");
        // simulating some hard work
        try {
            Thread.sleep(20);
        } catch (InterruptedException ignore) {}
        // stop after 5 cycles
        if (this.cycles() >= 5) {
            // return 'RTLIB_EXC_WORKLOAD_NONE' exit code
            throw new RTLibException(RTLibExitCode.RTLIB_EXC_WORKLOAD_NONE);
        }
    }

    @Override
    protected void onMonitor() throws RTLibException {
        System.out.println("[SimulatorEXC] onMonitor()");
    }

    @Override
    protected void onRelease() throws RTLibException {
        System.out.println("[SimulatorEXC] onRelease()");
    }
}
